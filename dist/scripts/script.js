console.log(111);

const header = document.querySelector(".page-header");
const toggleClass = "is-sticky";



window.onscroll = function() {myFunction()};

function myFunction() {
  if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 50) {
    header.classList.add(toggleClass);
  } else {
    header.classList.remove(toggleClass);
  }
}


const hamburger = document.querySelector(".hamburger");
const navMenu = document.querySelector(".navbar");

hamburger.addEventListener("click", mobileMenu);

function mobileMenu() {
    hamburger.classList.toggle("active");
    navMenu.classList.toggle("active");
}