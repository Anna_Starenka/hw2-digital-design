const gulp = require('gulp');
const htmlMin = require('gulp-htmlmin')

const concat = require('gulp-concat')
const jsMin = require('gulp-minify')

const cleanCSS = require('gulp-clean-css');

const clean = require('gulp-clean')

const browerSync = require('browser-sync').create();

// const imageMin = require('gulp-imagemin')


const sass = require('gulp-sass')(require('sass'));


const html = ()=>{
    return gulp.src('./src/*.html')
    .pipe(htmlMin({ collapseWhitespace: true }))
    .pipe(gulp.dest('./dist'))
}

const js = ()=>{
    return gulp.src('./src/scripts/**/*.js')
    .pipe(concat('script.js'))
    .pipe(jsMin({
        ext:{
            src:'.js',
            min:".min.js"
        }
    }))
    .pipe(gulp.dest('./dist/scripts'))
}

const css = () => {
    return gulp.src('./src/styles/**/*.css')
        .pipe(concat('style.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./dist/styles'))
}


const scss = () => {
    return gulp.src('./src/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(gulp.dest('./dist/styles'))
};

const server =()=>{
    browerSync.init({
        server:{
            baseDir: './dist'
        }
    })
}


const watcher =()=>{
    gulp.watch('./src/**/*.html', html).on('all', browerSync.reload)
    gulp.watch('./src/styles/**/*.{scss, sass, css}', scss).on('all', browerSync.reload)
    gulp.watch('./src/images/**/*.*', image).on('all', browerSync.reload)
    gulp.watch('./src/scripts/*.js', js).on('all',browerSync.reload);
}

const image =()=>{
    return gulp.src('./src/images/**/*.*')
    .pipe(gulp.dest('./dist/images'))
}

gulp.task('html', html);
gulp.task('image', image)
gulp.task('scss', scss)
gulp.task('browser-synk', server)


const cleanDist =()=>{
    return gulp.src('./dist', { read:false }).pipe(clean())
}

gulp.task('build', gulp.series(
    cleanDist,
    gulp.parallel(html,image, scss, js, css )
    ));



gulp.task('dev', gulp.series(
    gulp.parallel(html, image, scss, js, css),
    gulp.parallel(server, watcher)
));

